﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{

    [KnownType(typeof(STAFF_INFORMATION))]
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "StaffService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select StaffService.svc or StaffService.svc.cs at the Solution Explorer and start debugging.
    public class StaffService : IStaffService
    {
        public bool ifexist(String id)
        {

            List<STAFF_INFORMATION> staff;
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from s in ctx.STAFF_INFORMATION
                              select s;

                staff =  OraLINQ.ToList();

                foreach (STAFF_INFORMATION s in staff)
                {
                    if(s.S_ID.ToString().Equals(id))
                    {
                        return true;
                    }
                }
            }
            return false;


        }

        public STAFF_INFORMATION info(String id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryStaff = from staff in ctx.STAFF_INFORMATION
                                 select staff;
                 List<STAFF_INFORMATION> staff_list = queryStaff.ToList();

                foreach(STAFF_INFORMATION s in staff_list)
                {
                    if(s.S_ID.ToString().Equals(id))
                    {
                        return s;
                    }
                }

            }
            return null;
            
        }


        public bool creatstaff(String id, String name, String password, String gender, DateTime birthday, DateTime date, String telephone, String address)
        {
            var idnumber = Convert.ToDecimal(id);
            TJERPEntity ctx = new TJERPEntity();
           
            var OraLINQ = new STAFF_INFORMATION()
            {
                S_ID = idnumber,
                S_NAME = name,
                S_PASSWORD = password,
                S_GENDER = gender,
                S_BIRTHDAY = birthday,
                S_ICDATE = date,
                S_TEL = telephone,
                S_ADDRESS = address,
                S_MMABLE = 1,
                S_PPEABLE = 1,
                S_SDABLE = 1,
                S_FIABLE = 1
            };
            ctx.STAFF_INFORMATION.Add(OraLINQ);
            ctx.SaveChanges();
            return true;
        }



        
        public List<STAFF_INFORMATION> getAllStaffInfo()
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryStaff = from staff in ctx.STAFF_INFORMATION
                                 select staff;
                List<STAFF_INFORMATION> staff_list = queryStaff.ToList();
                return staff_list;
               
            }
            
        }



        public bool updateStaff(STAFF_INFORMATION s)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            ctx1.STAFF_INFORMATION.Attach(s);



            ctx1.Entry(s).State = System.Data.EntityState.Modified;

            ctx1.SaveChanges();



            return true;
        }


        public bool deleteStaff(String staffID)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedStaff = ctx1.STAFF_INFORMATION.Find(Convert.ToDecimal(staffID));

            ctx1.STAFF_INFORMATION.Attach(updatedStaff);
            ctx1.STAFF_INFORMATION.Remove(updatedStaff);
            ctx1.SaveChanges();
            return true;
        }





    }


    





}
