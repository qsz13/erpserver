﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPPService" in both code and config file together.
    [ServiceContract]
    public interface IPPService
    {

        [OperationContract]
        bool createWorkShop(String name, String materialGroup, String staffID, String managerID);

        [OperationContract]
        List<WORKSHOP> getWorkShop();

        [OperationContract]
        decimal createProductionPlan(String workshop, DateTime startTime, DateTime endTime, String planner, String charger, String production, String amount);

        [OperationContract]
        bool createFlowLine(String FPNum, String planID, String description, String material, String amount);


        [OperationContract]
        List<PRODUCTION_PLAN> getProductionPlanByWorkshop(String id);

        [OperationContract]
        PRODUCTION_PLAN getProductionPlan(String id);


        [OperationContract]
        List<FLOW_LINE_PROCEDURE> getFlowLineByProductionPlan(String id);

        [OperationContract]
        bool updateFlowLine(FLOW_LINE_PROCEDURE flowLine);

        [OperationContract]
        bool updateProduceState(String id, String condition);

        [OperationContract]
        bool updateProducePlanHaveAmountProduceAmount(String id, decimal haveAmount, decimal produceAmount);

        [OperationContract]
        bool updateMaterialLeftAmount(String id, decimal leftAmount);




    }
}
