﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CustomerService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CustomerService.svc or CustomerService.svc.cs at the Solution Explorer and start debugging.
    public class CustomerService : ICustomerService
    {
        public int createCustomer(String account, String company, String city, String country, String address, String code, int mg, int dc, int dp, int sc)
        {
            
            TJERPEntity ctx = new TJERPEntity();
           
            var OraLINQ = new CUSTOMER_INFORMATION()
            {
                C_BANKACCOUNT = account,C_NAME = company, C_COUNTRY = country, C_CITY = city,C_ADDRESS = address, C_POSTCODE = Convert.ToDecimal(code),C_DIVISION = mg,C_DCHANNEL=dc,C_DELIVPRIO = dp,C_SHIPCON = sc
            };

            ctx.CUSTOMER_INFORMATION.Add(OraLINQ);
            ctx.SaveChanges();
            return (int)OraLINQ.C_ID;
        }

        public CUSTOMER_INFORMATION getInfo(String id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryCustomer = from customer in ctx.CUSTOMER_INFORMATION
                                 select customer;
                List<CUSTOMER_INFORMATION> customer_list = queryCustomer.ToList();

                foreach (CUSTOMER_INFORMATION c in customer_list)
                {
                    if (c.C_ID.ToString() == id)
                    {
                        return c;
                    }
                }

            }
            return null;



        }


        public bool ifexist(String id)
        {
            List<CUSTOMER_INFORMATION> customer;
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from s in ctx.CUSTOMER_INFORMATION
                              select s;

                customer = OraLINQ.ToList();

                foreach (CUSTOMER_INFORMATION c in customer)
                {
                    if (c.C_ID.ToString().Equals(id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public CUSTOMER_INFORMATION getCustomerInfo(String id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryCustomer = from customer in ctx.CUSTOMER_INFORMATION
                                    select customer;
                List<CUSTOMER_INFORMATION> customer_list = queryCustomer.ToList();

                foreach (CUSTOMER_INFORMATION c in customer_list)
                {
                    if (c.C_ID.ToString().Equals(id))
                    {
                        return c;
                    }
                }

            }
            return null;



        }


        public  Dictionary<int, String> searchCustomer(String query)
        {
            var result = new Dictionary<int, String>();
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryCustomer = from customer in ctx.CUSTOMER_INFORMATION
                                    select customer;
                List<CUSTOMER_INFORMATION> customer_list = queryCustomer.ToList();

                foreach (CUSTOMER_INFORMATION c in customer_list)
                {
                    if (c.C_NAME.Contains(query))
                    {
                        int cid = Convert.ToInt32(c.C_ID);
                        result.Add(cid, c.C_NAME);
                    }
                }

            }
            return result;


        }


        public bool ifContactPersonExist(String id)
        {

            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryCustomer = from customer in ctx.CUSTOMER_INFORMATION
                                    select customer;
                List<CUSTOMER_INFORMATION> customer_list = queryCustomer.ToList();

                foreach (CUSTOMER_INFORMATION c in customer_list)
                {
                    if (c.C_ID.ToString().Equals(id))
                    {
                        if(c.C_CONPERSON == null)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }

            }


            return false;

        }

        public int createContactPerson(String customerID,String name, String gender, String tel, String department ,String function)
        {
            TJERPEntity ctx = new TJERPEntity();

            var OraLINQ = new CONTACT_PERSON()
            {
                CP_CUSTID = Convert.ToDecimal(customerID),
                CP_NAME = name,
                CP_GENDER = gender,
                CP_TEL = tel,
                CP_DEPARTMENT = department,
                CP_FUNCTION = function
            };

            ctx.CONTACT_PERSON.Add(OraLINQ);
            ctx.SaveChanges();





            decimal contactPersonID = (decimal)OraLINQ.CP_ID;
            
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedCustomer = ctx1.CUSTOMER_INFORMATION.Find(Convert.ToDecimal(customerID));
            updatedCustomer.C_CONPERSON = contactPersonID;
            ctx1.CUSTOMER_INFORMATION.Attach(updatedCustomer);
            var entry = ctx1.Entry(updatedCustomer);
            entry.Property(e => e.C_CONPERSON).IsModified = true;
            
            
            
            
            
            try
            {
                ctx1.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
  

            return (int)contactPersonID;
        }



 
    }
}

