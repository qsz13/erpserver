﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFIService" in both code and config file together.
    [ServiceContract]
    public interface IFIService
    {
        [OperationContract]
        bool updateAcountBalance(String accountID, String balance);

        [OperationContract]
        List<ACCOUNT> getAllAccountInformation();

        [OperationContract]
        ACCOUNT getAccountInformation(String accountID);


        [OperationContract]
        List<CAPITAL_IN> getCapInByTime(DateTime startTime, DateTime endTime);

        [OperationContract]
        List<CAPITAL_OUT> getCapOutByTime(DateTime startTime, DateTime endTime);

        [OperationContract]
        CAPITAL_IN  getCapital_In(String CI_ID);

        [OperationContract]
        CAPITAL_OUT getCapital_Out(String CO_ID);
    }
}
