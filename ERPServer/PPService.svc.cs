﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PPService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PPService.svc or PPService.svc.cs at the Solution Explorer and start debugging.
    public class PPService : IPPService
    {

        public bool createWorkShop(String name, String materialGroup, String staffID, String managerID)
        {
            TJERPEntity ctx = new TJERPEntity();
            var OraLINQ = new WORKSHOP()
            {
                WS_NAME = name,
                WS_MANAGERID = Convert.ToDecimal(managerID),
                WS_STAFFNUM = Convert.ToDecimal(staffID),
                WS_MATERIALGROUP = Convert.ToDecimal(materialGroup),
                STAFF_INFORMATION = null,
                PRODUCTION_PLAN = null,
                MATERIAL_GROUP = null
            };
            ctx.WORKSHOPs.Add(OraLINQ);
            ctx.SaveChanges();
            return true;
        }

        public List<WORKSHOP> getWorkShop()
        {
            var result = new List<WORKSHOP>();
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryWorkShop = from c in ctx.WORKSHOPs
                                       select c;
                List<WORKSHOP> cinf_list = queryWorkShop.ToList();

                foreach (WORKSHOP c in cinf_list)
                {
                        result.Add(c);
                }
                return result;

            }
        }

        public decimal createProductionPlan(String workshop, DateTime startTime, DateTime endTime, String planner, String charger, String production, String amount)
        {


            TJERPEntity ctx = new TJERPEntity();

            var OraLINQ = new PRODUCTION_PLAN()
            {
                PP_WORKSHOP = Convert.ToDecimal(workshop),
                PP_STARTTIME = startTime,
                PP_ENDTIME = endTime,
                PP_PLANNER = Convert.ToDecimal(planner),
                PP_CHARGER = Convert.ToDecimal(charger),
                PP_PRODUCTION = Convert.ToDecimal(production),
                PP_PLANAMOUNT = Convert.ToDecimal(amount),
                PP_INAMOUNT = 0,
                PP_PRODUCEDAMOUNT = 0,
                PP_PRODUCESTATE = 0,
                MATERIAL  = null,
                FLOW_LINE_PROCEDURE = null,
                PRODUCTION_CONDITION = null,
                WORKSHOP =  null,
                STAFF_INFORMATION = null
                

            };
            ctx.PRODUCTION_PLAN.Add(OraLINQ);
            ctx.SaveChanges();

            return OraLINQ.PP_ID;
        }

        public bool createFlowLine(String FPNum, String planID, String description, String material, String amount)
        {

            TJERPEntity ctx = new TJERPEntity();

            var OraLINQ = new FLOW_LINE_PROCEDURE()
            {
                FP_NUM = Convert.ToDecimal(FPNum),
                FP_PPID = Convert.ToDecimal(planID),
                FP_DESCRIPTION = description,
                FP_MATERIAL = Convert.ToDecimal(material),
                FP_AMOUNT = Convert.ToDecimal(amount),
                FP_HAVEAMOUNT = 0,
                FP_MADEAMOUNT = 0

            };
            ctx.FLOW_LINE_PROCEDURE.Add(OraLINQ);
            ctx.SaveChanges();
            return true;
        }

        public List<PRODUCTION_PLAN> getProductionPlanByWorkshop(String id)
        {
            List<PRODUCTION_PLAN> resultList = new List<PRODUCTION_PLAN>();


            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryProductionPlan = from c in ctx.PRODUCTION_PLAN
                                    select c;
                List<PRODUCTION_PLAN> production_plan_list = queryProductionPlan.ToList();

                foreach (PRODUCTION_PLAN p in production_plan_list)
                {
                    if(p.PP_WORKSHOP.ToString().Equals(id))
                    {
                        resultList.Add(p);
                    }
                    
                }

                return resultList;
            }

        }

        public List<FLOW_LINE_PROCEDURE> getFlowLineByProductionPlan(String id)
        {
            var result = new List<FLOW_LINE_PROCEDURE>();

            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryProductionPlan = from f in ctx.FLOW_LINE_PROCEDURE
                                          select f;
                List<FLOW_LINE_PROCEDURE> flow_line_list = queryProductionPlan.ToList();

                foreach (FLOW_LINE_PROCEDURE p in flow_line_list)
                {
                    if (p.FP_PPID.ToString().Equals(id))
                    {
                        result.Add(p);
                    }

                }

                return result;
            }



            return result;
        }

        public bool updateFlowLine(FLOW_LINE_PROCEDURE flowLine)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedFlowLine = ctx1.FLOW_LINE_PROCEDURE.Find(flowLine.FP_ID);
            updatedFlowLine.FP_HAVEAMOUNT = flowLine.FP_HAVEAMOUNT;
            updatedFlowLine.FP_MADEAMOUNT = flowLine.FP_MADEAMOUNT;
            ctx1.FLOW_LINE_PROCEDURE.Attach(updatedFlowLine);
            var entry = ctx1.Entry(updatedFlowLine);
            entry.Property(e => e.FP_HAVEAMOUNT).IsModified = true;
            entry.Property(e => e.FP_MADEAMOUNT).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }


        public bool updateProduceState(String id,String condition)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedProductionPlan = ctx1.PRODUCTION_PLAN.Find(Convert.ToDecimal(id));
            updatedProductionPlan.PP_PRODUCESTATE = Convert.ToDecimal(condition);
            ctx1.PRODUCTION_PLAN.Attach(updatedProductionPlan);
            var entry = ctx1.Entry(updatedProductionPlan);
            entry.Property(e => e.PP_PRODUCESTATE).IsModified = true;
            ctx1.SaveChanges();
              return true;
        }

        public bool updateProducePlanHaveAmountProduceAmount(String id, decimal haveAmount, decimal produceAmount)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedProductionPlan = ctx1.PRODUCTION_PLAN.Find(Convert.ToDecimal(id));
            updatedProductionPlan.PP_INAMOUNT = haveAmount;
            updatedProductionPlan.PP_PRODUCEDAMOUNT = produceAmount;
            ctx1.PRODUCTION_PLAN.Attach(updatedProductionPlan);
            var entry = ctx1.Entry(updatedProductionPlan);
            entry.Property(e => e.PP_INAMOUNT).IsModified = true;
            entry.Property(e => e.PP_PRODUCEDAMOUNT).IsModified = true;

            ctx1.SaveChanges();
            return true;

        }


        public bool updateMaterialLeftAmount(String id, decimal leftAmount)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedMaterial = ctx1.MATERIALs.Find(Convert.ToDecimal(id));
            updatedMaterial.M_LEFTAMOUT = leftAmount;

            ctx1.MATERIALs.Attach(updatedMaterial);
            var entry = ctx1.Entry(updatedMaterial);
            entry.Property(e => e.M_LEFTAMOUT).IsModified = true;

            ctx1.SaveChanges();
            return true;



        }

        public PRODUCTION_PLAN getProductionPlan(String id)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryProductionPlan = from c in ctx.PRODUCTION_PLAN
                                          select c;
                List<PRODUCTION_PLAN> production_plan_list = queryProductionPlan.ToList();

                foreach (PRODUCTION_PLAN p in production_plan_list)
                {
                    if (p.PP_ID.ToString().Equals(id))
                    {
                        return p;
                    }

                }

                return null;
            }
        }

    }
}
