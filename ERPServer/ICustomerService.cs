﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICustomerService" in both code and config file together.
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        int createCustomer(String account, String company, String city, String country, String address, String code, int mg, int dc, int dp, int sc);

        [OperationContract]
        CUSTOMER_INFORMATION getInfo(String id);

        [OperationContract]
        bool ifexist(String id);

        [OperationContract]
        CUSTOMER_INFORMATION getCustomerInfo(String id);

        [OperationContract]
        Dictionary<int, String> searchCustomer(String query);

        [OperationContract]
        bool ifContactPersonExist(String id);

        [OperationContract]
        int createContactPerson(String customerID,String name, String gender, String tel, String department ,String function);
    
    }
}
