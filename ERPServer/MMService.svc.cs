using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MMService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MMService.svc or MMService.svc.cs at the Solution Explorer and start debugging.
    public class MMService : IMMService
    {
        public Dictionary<int, String> getAllMaterialGroup()
        {
            Dictionary<int, String> result = new Dictionary<int, String>();

            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryMaterialGroup = from mg in ctx.MATERIAL_GROUP
                                         select mg;

                foreach (MATERIAL_GROUP mg in queryMaterialGroup)
                {
                    result.Add((int)mg.MG_ID, mg.MG_NAME);
                }

                return result;
            }
        }


        public bool ifMaterialExist(String id)
        {
            List<MATERIAL> material;
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.MATERIALs
                              select m;

                material = OraLINQ.ToList();

                foreach (MATERIAL m in material)
                {
                    if (m.M_MATERIALID.ToString().Equals(id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public bool createMaterial(String id, String name, String mgid, String description, String cost, String price, String leftAmount)
        {

            TJERPEntity ctx = new TJERPEntity();
            if (price.Equals(""))
            {

                var OraLINQ = new MATERIAL()
                {
                    M_MATERIALID = Convert.ToDecimal(id),
                    M_NAME = name,
                    M_MGID = Convert.ToDecimal(mgid),
                    M_DESCRIPTION = description,
                    M_COST = Convert.ToDecimal(cost),
                    M_PRICE = null,
                    M_LEFTAMOUT = Convert.ToDecimal(leftAmount)
                };

                ctx.MATERIALs.Add(OraLINQ);


            }
            else
            {
                var OraLINQ = new MATERIAL()
                {
                    M_MATERIALID = Convert.ToDecimal(id),
                    M_NAME = name,
                    M_MGID = Convert.ToDecimal(mgid),
                    M_DESCRIPTION = description,
                    M_COST = Convert.ToDecimal(cost),
                    M_PRICE = Convert.ToDecimal(price),
                    M_LEFTAMOUT = Convert.ToDecimal(leftAmount)
                };
                ctx.MATERIALs.Add(OraLINQ);

            }
            ctx.SaveChanges();
            return true;
        }


        public MATERIAL getMaterial(String id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryMaterial = from material in ctx.MATERIALs
                                    select material;
                List<MATERIAL> material_list = queryMaterial.ToList();

                foreach (MATERIAL m in material_list)
                {
                    if (m.M_MATERIALID.ToString() == id)
                    {
                        return m;
                    }
                }

            }
            return null;
        }

        public PURCHASE_ORDER getPurchseOrder(String purchaseOrder_id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryPurchaseOrder = from purchaseOrder in ctx.PURCHASE_ORDER
                                         select purchaseOrder;
                List<PURCHASE_ORDER> purchase_list = queryPurchaseOrder.ToList();

                foreach (PURCHASE_ORDER p in purchase_list)
                {
                    if (p.PO_ID == Convert.ToDecimal(purchaseOrder_id))
                    {
                        return p;
                    }
                }
            }
            return null;
        }

        public bool ifPurchaseOrderExist(String id)
        {
            List<PURCHASE_ORDER> PurchaseOrder;
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.PURCHASE_ORDER
                              select m;

                PurchaseOrder = OraLINQ.ToList();

                foreach (PURCHASE_ORDER p in PurchaseOrder)
                {
                    if (p.PO_ID.ToString().Equals(id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<VENDOR_INFORMATION> searchVenderID(String venderNamePiece)
        {
            var result = new List<VENDOR_INFORMATION>();
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryVenderInf = from venderinf in ctx.VENDOR_INFORMATION
                                     select venderinf;
                List<VENDOR_INFORMATION> venderinf_list = queryVenderInf.ToList();

                foreach (VENDOR_INFORMATION v in venderinf_list)
                {
                    if (v.VI_NAME.ToString().Contains(venderNamePiece))
                    {

                        result.Add(v);
                    }
                }
                return result;

            }

        }

        public VENDOR_INFORMATION getVenderInformation(String vender_id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryVenderInf = from venderinf in ctx.VENDOR_INFORMATION
                                     select venderinf;
                List<VENDOR_INFORMATION> venderinf_list = queryVenderInf.ToList();

                foreach (VENDOR_INFORMATION v in venderinf_list)
                {
                    if (v.VI_ID.ToString().Equals(vender_id))
                    {
                        return v;
                    }
                }

            }
            return null;

        }

        public bool creatVenderInformation(String name, String country, String city, String address, int postcode, String account, String Tel)
        {
            TJERPEntity ctx = new TJERPEntity();
            var OraLINQ = new VENDOR_INFORMATION()
            {
                VI_NAME = name,
                VI_COUNTRY = country,
                VI_CITY = city,
                VI_ADDRESS = address,
                VI_POSTCODE = Convert.ToDecimal(postcode),
                VI_ACCOUNT = account,
                VI_TEL = Tel
            };
            ctx.VENDOR_INFORMATION.Add(OraLINQ);
            ctx.SaveChanges();
            return true;
        }

        public List<ACCOUNT> getAccountInformation()
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.ACCOUNTs
                              select m;

                List<ACCOUNT> accounts_list = OraLINQ.ToList();

                return accounts_list;

            }
        }

        public bool createCapitalOut(DateTime time, String inAccount, String fromAccount, String amount, String descreption, String outStaffID, String relatedPurchaseOrderID)
        {
            TJERPEntity ctx = new TJERPEntity();
            var FromAccount = ctx.ACCOUNTs.Find(fromAccount);
            var StaffInfo = ctx.STAFF_INFORMATION.Find(Convert.ToDecimal(outStaffID));

            var PurchaseOrder = ctx.PURCHASE_ORDER.Find(Convert.ToDecimal(relatedPurchaseOrderID));
            
            var OraLINQ = new CAPITAL_OUT()
            {
                CO_ID = "",
                CO_TIME = time,
                CO_INTOACCOUNTNUM = inAccount,
                CO_FROMACCOUNTNUM = fromAccount,
                CO_AMOUNT = Convert.ToDecimal(amount),
                CO_DESCRIPTION = descreption,
                CO_OUTSTAFFID = Convert.ToDecimal(outStaffID),
                CO_RELATEDPURCHASENUM = Convert.ToDecimal(relatedPurchaseOrderID),
                ACCOUNT = FromAccount,
                STAFF_INFORMATION = StaffInfo,
                PURCHASE_ORDER = PurchaseOrder
            };
            ctx.CAPITAL_OUT.Add(OraLINQ);
            
            //try
            //{
              ctx.SaveChanges();                 
            //}
            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}
            
            return true;

        }

        public bool updatePOPaidMoney(String paidMoney, String purchaseOrder_id)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedPaidMoney = ctx1.PURCHASE_ORDER.Find(Convert.ToDecimal(purchaseOrder_id));
            updatedPaidMoney.PO_PAIDMONEY += Convert.ToDecimal(paidMoney);
            ctx1.PURCHASE_ORDER.Attach(updatedPaidMoney);
            var entry = ctx1.Entry(updatedPaidMoney);
            entry.Property(e => e.PO_PAIDMONEY).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }

        public bool updatePOAllPaidTime(DateTime allPaidTime, String purchaseOrder_id)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedPaidTime = ctx1.PURCHASE_ORDER.Find(Convert.ToDecimal(purchaseOrder_id));
            updatedPaidTime.PO_PAYTIME = allPaidTime;
            ctx1.PURCHASE_ORDER.Attach(updatedPaidTime);
            var entry = ctx1.Entry(updatedPaidTime);
            entry.Property(e => e.PO_PAYTIME).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }


        public List<PURCHASE_ORDER> getPOByVender(String venderID)
        {
            List<PURCHASE_ORDER> temp = new List<PURCHASE_ORDER>();
            using (TJERPEntity ctx1 = new TJERPEntity())
            {
                var OraLINQ = from m in ctx1.PURCHASE_ORDER
                              select m;

                List<PURCHASE_ORDER> PurchaseOrder_list = OraLINQ.ToList();

                foreach (PURCHASE_ORDER p in PurchaseOrder_list)
                {
                    if (p.PO_VENDORID.ToString().Equals(venderID))
                    {
                        temp.Add(p);
                    }
                }
                return temp;
            }
        }

        public bool changeAmount(String id, String numl, bool ifailed) // buwen
        {
            return true;
        }


        public bool updateCondition(int condition, String purchaseOrder_id)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatePOCondition = ctx1.PURCHASE_ORDER.Find(Convert.ToDecimal(purchaseOrder_id));
            updatePOCondition.PO_PORDERSTATE = Convert.ToDecimal(condition);
            ctx1.PURCHASE_ORDER.Attach(updatePOCondition);
            var entry = ctx1.Entry(updatePOCondition);
            entry.Property(e => e.PO_PORDERSTATE).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }

        public List<MATERIAL> getAllMaterial()
        {
            List<MATERIAL> temp = new List<MATERIAL>();
            using (TJERPEntity ctx1 = new TJERPEntity())
            {
                var OraLINQ = from m in ctx1.MATERIALs
                              select m;

                List<MATERIAL> material_list = OraLINQ.ToList();

                foreach (MATERIAL p in material_list)
                {
                    temp.Add(p);
                }
                return temp;
            }
        }


        public bool ifVenderExist(String vendor_id)
        {
            List<VENDOR_INFORMATION> vendor;
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.VENDOR_INFORMATION
                              select m;

                vendor = OraLINQ.ToList();

                foreach (VENDOR_INFORMATION v in vendor)
                {
                    if (v.VI_ID.ToString().Equals(vendor_id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool createPurchaseOrder(String vender_id, String downPayment, String totalPrice, String SDStaffID)
        {
            TJERPEntity ctx = new TJERPEntity();
            var OraLINQ = new PURCHASE_ORDER()
            {
                PO_VENDORID = Convert.ToDecimal(vender_id),
                PO_DOWNPAYMENT = Convert.ToDecimal(downPayment),
                PO_TOTALPRICE = Convert.ToDecimal(totalPrice),
                PO_SDSTAFFID = Convert.ToDecimal(SDStaffID),
                CAPITAL_OUT = null,
                PURCHASE_ITEM = null
            };
            ctx.PURCHASE_ORDER.Add(OraLINQ);
            ctx.SaveChanges();
            return true;

        }
        public SALES_ORDER getSalesOrder(String id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var querySalesOrder = from o in ctx.SALES_ORDER
                                      select o;
                List<SALES_ORDER> sales_list = querySalesOrder.ToList();

                foreach (SALES_ORDER s in sales_list)
                {
                    if (s.SO_ID == Convert.ToDecimal(id))
                    {
                        return s;
                    }
                }
            }
            return null;
        }


        public bool ifSalesOrderExist(String id)
        {
            List<SALES_ORDER> SalesOrder;
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.SALES_ORDER
                              select m;

                SalesOrder = OraLINQ.ToList();

                foreach (SALES_ORDER s in SalesOrder)
                {
                    if (s.SO_ID.ToString().Equals(id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        //[OperationContract]
        public List<CUSTOMER_INFORMATION> searchCustomerID(String customerNamePiece)
        {
            var result = new List<CUSTOMER_INFORMATION>();
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryCustomerInf = from c in ctx.CUSTOMER_INFORMATION
                                     select c;
                List<CUSTOMER_INFORMATION> cinf_list = queryCustomerInf.ToList();

                foreach (CUSTOMER_INFORMATION c in cinf_list)
                {
                    if (c.C_NAME.ToString().Contains(customerNamePiece))
                    {

                        result.Add(c);
                    }
                }
                return result;

            }
        }

        
        public List<SALES_ORDER> getSOByCustomer(String CustomerID)
        {
            List<SALES_ORDER> temp = new List<SALES_ORDER>();
            using (TJERPEntity ctx1 = new TJERPEntity())
            {
                var OraLINQ = from m in ctx1.SALES_ORDER
                              select m;

                List<SALES_ORDER> SalesOrder_list = OraLINQ.ToList();

                foreach (SALES_ORDER s in SalesOrder_list)
                {
                    if (s.SO_CUSID.ToString().Equals(CustomerID))
                    {
                        temp.Add(s);
                    }
                }
                return temp;
            }
        }

        public bool updateSOGetMoney(String getMoney, String SalesOrder_id)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedGetMoney = ctx1.SALES_ORDER.Find(Convert.ToDecimal(SalesOrder_id));
            updatedGetMoney.SO_PAIDMONEY += Convert.ToDecimal(getMoney);
            ctx1.SALES_ORDER.Attach(updatedGetMoney);
            var entry = ctx1.Entry(updatedGetMoney);
            entry.Property(e => e.SO_PAIDMONEY).IsModified = true;
            ctx1.SaveChanges();
            return true;

        }

        public bool createCapitalIn(DateTime time, String inAccount, String toAccount, String amount, String descreption, String inStaffID, String salesOrderID)
        {
            TJERPEntity ctx = new TJERPEntity();
            var Account = ctx.ACCOUNTs.Find(inAccount);

            var StaffInfo = ctx.STAFF_INFORMATION.Find(Convert.ToDecimal(inStaffID));

            if(salesOrderID!=null)
            {
                var SalesOrder = ctx.SALES_ORDER.Find(Convert.ToDecimal(salesOrderID));
                var OraLINQ1 = new CAPITAL_IN()
                {
                    CI_TIME = time,
                    CI_INACCOUNTNUM = inAccount,
                    CI_TOACCOUNTNUM = toAccount,
                    CI_AMOUNT = Convert.ToDecimal(amount),
                    CI_DESCRIPTION = descreption,
                    CI_INSTAFFID = Convert.ToDecimal(inStaffID),
                    CI_SALESORDERID = Convert.ToDecimal(salesOrderID),
                    ACCOUNT = Account,
                    STAFF_INFORMATION = StaffInfo,
                    SALES_ORDER = SalesOrder
                };
                ctx.CAPITAL_IN.Add(OraLINQ1);
            }
            else
            {
                var OraLINQ1 = new CAPITAL_IN()
                {
                    CI_TIME = time,
                    CI_INACCOUNTNUM = inAccount,
                    CI_TOACCOUNTNUM = toAccount,
                    CI_AMOUNT = Convert.ToDecimal(amount),
                    CI_DESCRIPTION = descreption,
                    CI_INSTAFFID = Convert.ToDecimal(inStaffID),
                    CI_SALESORDERID = null,
                    ACCOUNT = Account,
                    STAFF_INFORMATION = StaffInfo,
                    SALES_ORDER = null
                };
                ctx.CAPITAL_IN.Add(OraLINQ1);
            }

          


            ctx.SaveChanges();
            return true;
        }


    public bool updateStoreAmount(String PI_ID, String amount)
    {
        TJERPEntity ctx1 = new TJERPEntity();
        var updateStoreAmount = ctx1.PURCHASE_ITEM.Find(Convert.ToDecimal(PI_ID));
        decimal result = (decimal)updateStoreAmount.PI_STOREAMOUNT + Convert.ToDecimal(amount);
        updateStoreAmount.PI_STOREAMOUNT = result;
        ctx1.PURCHASE_ITEM.Attach(updateStoreAmount);
        var entry = ctx1.Entry(updateStoreAmount);
        entry.Property(e => e.PI_STOREAMOUNT).IsModified = true;
        ctx1.SaveChanges();
        return true;
    }

    public List<PURCHASE_ITEM> getPurchaseItem(String purchaseOrder_id)
    {
        List<PURCHASE_ITEM> result = new List<PURCHASE_ITEM>();
        using (TJERPEntity ctx1 = new TJERPEntity())
        {
            var OraLINQ = from m in ctx1.PURCHASE_ITEM
                          select m;

            List<PURCHASE_ITEM> purchaseItem_list = OraLINQ.ToList();

            foreach (PURCHASE_ITEM pi in purchaseItem_list)
            {
                if (pi.PI_PURORDERID.ToString().Equals(purchaseOrder_id))
                {
                    result.Add(pi);
                }
            }
            return result;
        }
 
    }

    public bool ifReceivedAll(String PI_ID)
    {
        TJERPEntity ctx = new TJERPEntity();
        PURCHASE_ITEM pi = ctx.PURCHASE_ITEM.Find(Convert.ToDecimal(PI_ID));
        if (pi.PI_RECEIVEAMOUNT >= pi.PI_PREAMOUNT)
            return true;
        else return false;
    }


 public bool updateReceivedAmount(String PI_ID, String amount)
    {
        TJERPEntity ctx1 = new TJERPEntity();
        var updateReceivedAmount = ctx1.PURCHASE_ITEM.Find(Convert.ToDecimal(PI_ID));
        decimal result = (decimal)updateReceivedAmount.PI_RECEIVEAMOUNT + Convert.ToDecimal(amount);
        updateReceivedAmount.PI_RECEIVEAMOUNT = result;
        ctx1.PURCHASE_ITEM.Attach(updateReceivedAmount);
        var entry = ctx1.Entry(updateReceivedAmount);
        entry.Property(e => e.PI_RECEIVEAMOUNT).IsModified = true;
        ctx1.SaveChanges();
        return true;
    }

   //public List<PURCHASE_ITEM> getPurchaseItem(String purchaseOrder_id)
   // {
   //     List<PURCHASE_ITEM> result = new List<PURCHASE_ITEM>();
   //     using (TJERPEntity ctx1 = new TJERPEntity())
   //     {
   //         var OraLINQ = from m in ctx1.PURCHASE_ITEM
   //                       select m;

   //         List<PURCHASE_ITEM> purchaseItem_list = OraLINQ.ToList();

   //         foreach (PURCHASE_ITEM pi in purchaseItem_list)
   //         {
   //             if (pi.PI_PURORDERID.ToString().Equals(purchaseOrder_id))
   //             {
   //                 result.Add(pi);
   //             }
   //         }
   //         return result;
   //     }
 
   // }

        public List<String> getMaterialGroupNo()
    {
        List<String> temp = new List<String>();
        using (TJERPEntity ctx1 = new TJERPEntity())
        {
            var OraLINQ = from m in ctx1.MATERIAL_GROUP
                          select m;

            List<MATERIAL_GROUP> material_list = OraLINQ.ToList();

            foreach (MATERIAL_GROUP mg in material_list)
            {
                temp.Add(mg.MG_ID.ToString());
            }
            return temp;
        }
       
    }

    }




}
