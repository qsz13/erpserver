using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMMService" in both code and config file together.
    [ServiceContract]
    public interface IMMService
    {
        [OperationContract]
        Dictionary<int, String> getAllMaterialGroup();

        [OperationContract]
        bool ifMaterialExist(String id);

        [OperationContract]
        bool createMaterial(String id, String name, String mgid, String description, String cost, String price, String leftAmount);

        [OperationContract]
        MATERIAL getMaterial(String id);



        [OperationContract]
        PURCHASE_ORDER getPurchseOrder(String purchaseOrder_id);

        [OperationContract]
        bool ifPurchaseOrderExist(String purchaseOrder_id);

        [OperationContract]
        List<VENDOR_INFORMATION> searchVenderID(String venderNamePiece);

        [OperationContract]
        VENDOR_INFORMATION getVenderInformation(String vender_id);

        [OperationContract]
        bool creatVenderInformation(String name, String country, String city, String address, int postcode, String account, String Tel);

        [OperationContract]
        List<ACCOUNT> getAccountInformation();

        [OperationContract]
        bool createCapitalOut(DateTime time, String inAccount, String fromAccount, String amount, String descreption, String outStaffID, String relatedPurchaseOrderID);

        [OperationContract]
        bool updatePOPaidMoney(String paidMoney, String purchaseOrder_id);

        [OperationContract]
        bool updatePOAllPaidTime(DateTime allPaidTime, String purchaseOrder_id);

        [OperationContract]
        List<PURCHASE_ORDER> getPOByVender(String venderID);

        [OperationContract]
        bool changeAmount(String id, String numl, bool ifailed);

        [OperationContract]
        bool updateCondition(int condition, String purchaseOrder_id);

        [OperationContract]
        List<MATERIAL> getAllMaterial();



        [OperationContract]
        bool ifVenderExist(String vendor_id);

        [OperationContract]
        bool createPurchaseOrder(String vender_id, String downPayment, String totalPrice, String SDStaffID);

        [OperationContract]//新加的
        List<String> getMaterialGroupNo();

        [OperationContract]//新加的
        List<PURCHASE_ITEM> getPurchaseItem(String purchaseOrder_id);

        [OperationContract]//新加的
        bool updateReceivedAmount(String PI_ID, String amount);

        [OperationContract]//新加的
        bool ifReceivedAll(String PI_ID);

        [OperationContract]
        SALES_ORDER getSalesOrder(String id);

        [OperationContract]
        bool ifSalesOrderExist(String id);

        [OperationContract]
        List<CUSTOMER_INFORMATION> searchCustomerID(String customerNamePiece);

        [OperationContract]
        List<SALES_ORDER> getSOByCustomer(String CustomerID);

        [OperationContract]
        bool createCapitalIn(DateTime time, String inAccount, String toAccount, String amount, String descreption, String inStaffID, String salesOrderID);

        [OperationContract]
        bool updateSOGetMoney(String getMoney, String SalesOrder_id);

        [OperationContract]
        bool updateStoreAmount(String PI_ID, String amount);

    }
}
