﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IStaffService" in both code and config file together.
    [ServiceContract]
    public interface IStaffService
    {
        [OperationContract]
        bool ifexist(String id);

        [OperationContract]
        STAFF_INFORMATION info(String id);

        [OperationContract]
        bool creatstaff(String id, String name, String password, String gender, DateTime birthday, DateTime date, String telephone, String address);


        [OperationContract]
        List<STAFF_INFORMATION> getAllStaffInfo();

        [OperationContract]
        bool updateStaff(STAFF_INFORMATION s);

        [OperationContract]
        bool deleteStaff(String staffID);

    }
}
