//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ERPServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class MATERIAL_GROUP
    {
        public MATERIAL_GROUP()
        {
            this.CUSTOMER_INFORMATION = new HashSet<CUSTOMER_INFORMATION>();
            this.MATERIALs = new HashSet<MATERIAL>();
            this.WORKSHOPs = new HashSet<WORKSHOP>();
        }
    
        public decimal MG_ID { get; set; }
        public string MG_NAME { get; set; }
        public string MG_DESCRIPTION { get; set; }
    
        public virtual ICollection<CUSTOMER_INFORMATION> CUSTOMER_INFORMATION { get; set; }
        public virtual ICollection<MATERIAL> MATERIALs { get; set; }
        public virtual ICollection<WORKSHOP> WORKSHOPs { get; set; }
    }
}
