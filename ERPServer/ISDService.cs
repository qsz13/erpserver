﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISDService" in both code and config file together.
    [ServiceContract]
    public interface ISDService
    {
        [OperationContract]
        Dictionary<int, String> getAllDistributionChannel();

        [OperationContract]
        Dictionary<int, String> getAllDeliveryPriority();

        [OperationContract]
        Dictionary<int, String> getAllShipCondition();

        [OperationContract]
        int createSalesOrder(String customerID, DateTime deadline, String staffID, String DownPayment, int totalPrice);

        [OperationContract]
        void createDeliveryItem(String salesOrderID, String materialID, String amount, String itemPrice);

        [OperationContract]
        SALES_ORDER getSalesOrder(String id);

        [OperationContract]
        PURCHASE_ORDER getPurchaseOrder(String id);

        [OperationContract]
        bool updateSalesOrder(String id , int status);

        [OperationContract]
        bool updatePurchaseOrder(String id, int status);



        [OperationContract]
        List<DELIVERY_ITEM> getDeliveryItemList(String SO_ID);

        [OperationContract]//新加的
        bool updateDeliveryItemAmount(String DI_ID, String amount);

        [OperationContract]//新加的
        bool updateMaterial(String M_ID, String amount);


    }
}
