﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FIService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FIService.svc or FIService.svc.cs at the Solution Explorer and start debugging.
    public class FIService : IFIService
    {
        public bool updateAcountBalance(String accountID,String balance)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedAccount = ctx1.ACCOUNTs.Find(accountID);
            updatedAccount.A_BALANCE = balance;
            ctx1.ACCOUNTs.Attach(updatedAccount);
            var entry = ctx1.Entry(updatedAccount);
            entry.Property(e => e.A_BALANCE).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }



        public List<ACCOUNT> getAllAccountInformation()
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.ACCOUNTs
                              select m;

                List<ACCOUNT> accounts_list = OraLINQ.ToList();

                return accounts_list;

            }
        }

        public ACCOUNT getAccountInformation(String accountID)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.ACCOUNTs
                              select m;

                List<ACCOUNT> accounts_list = OraLINQ.ToList();
                foreach(ACCOUNT a in accounts_list)
                {
                    if(a.A_NUMBER.Equals(accountID))
                    {
                        return a;
                    }
                }
                
                

                return null;

            }
        }


        public List<CAPITAL_IN> getCapInByTime(DateTime startTime, DateTime endTime)
        {
            List<CAPITAL_IN> result = new List<CAPITAL_IN>();


            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.CAPITAL_IN
                              select m;

                List<CAPITAL_IN> capInList = OraLINQ.ToList();
                foreach (CAPITAL_IN cin in capInList)
                {
                    DateTime? ctime = cin.CI_TIME;

                    if (ctime > startTime && ctime < endTime)
                    {
                        result.Add(cin);
                    }
                }



                return result;

            }



        }

        public List<CAPITAL_OUT> getCapOutByTime(DateTime startTime, DateTime endTime)
        {
            List<CAPITAL_OUT> result = new List<CAPITAL_OUT>();


            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.CAPITAL_OUT
                              select m;

                List<CAPITAL_OUT> capOutList = OraLINQ.ToList();
                foreach (CAPITAL_OUT cout in capOutList)
                {
                    DateTime? ctime = cout.CO_TIME;

                    if (ctime > startTime && ctime < endTime)
                    {
                        result.Add(cout);
                    }
                }



                return result;

            }
        }


        public CAPITAL_IN getCapital_In(String CI_ID)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.CAPITAL_IN
                              select m;

                List<CAPITAL_IN> capInList = OraLINQ.ToList();
                foreach (CAPITAL_IN cin in capInList)
                {

                    if (cin.CI_ID.ToString().Equals(CI_ID))
                    {
                        return cin;
                    }
                }
                return null;

            }
 
        }

        public CAPITAL_OUT getCapital_Out(String CO_ID)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var OraLINQ = from m in ctx.CAPITAL_OUT
                              select m;

                List<CAPITAL_OUT> capOutList = OraLINQ.ToList();
                foreach (CAPITAL_OUT cout in capOutList)
                {

                    if (cout.CO_ID.ToString().Equals(CO_ID))
                    {
                        return cout;
                    }
                }
                return null;

            }
        }





    }
}
