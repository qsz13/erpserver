﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ERPServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SDService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SDService.svc or SDService.svc.cs at the Solution Explorer and start debugging.
    public class SDService : ISDService
    {
        public Dictionary<int, String> getAllDistributionChannel()
        {
            Dictionary<int, String> result = new Dictionary<int, String>();

            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryMaterialGroup = from dc in ctx.DISTRIBUTION_CHANNEL
                                         select dc;

                foreach (DISTRIBUTION_CHANNEL dc in queryMaterialGroup)
                {
                    result.Add((int)dc.DC_ID, dc.DC_NAME);
                }

                return result;
            }
        }


        public Dictionary<int, String> getAllDeliveryPriority()
        {
            Dictionary<int, String> result = new Dictionary<int, String>();

            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryMaterialGroup = from dp in ctx.DELIVERY_PRIORITY
                                         select dp;

                foreach (DELIVERY_PRIORITY dp in queryMaterialGroup)
                {
                    result.Add((int)dp.DP_DELIPRIORITY, dp.DP_DESCRIPTION);
                }

                return result;
            }
        }

        public Dictionary<int, String> getAllShipCondition()
        {
            Dictionary<int, String> result = new Dictionary<int, String>();

            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryMaterialGroup = from sc in ctx.SHIPPING_CONDITION
                                         select sc;

                foreach (SHIPPING_CONDITION sc in queryMaterialGroup)
                {
                    result.Add((int)sc.SC_SHIPCONDITION, sc.SC_DESCRIPTION);
                }

                return result;
            }
        }

        public int createSalesOrder(String customerID, DateTime deadline, String staffID, String DownPayment, int totalPrice)
        {

            TJERPEntity ctx = new TJERPEntity();

            var OraLINQ = new SALES_ORDER()
            {
                SO_CUSID  = Convert.ToDecimal(customerID),
                SO_ORDERDATE = DateTime.Now,
                SO_DEADLINE = deadline,
                SO_SDSTAFFID = Convert.ToDecimal(staffID),
                SO_DOWNPAYMENT = Convert.ToDecimal(DownPayment),
                SO_TOTALPRICE = totalPrice,
                SO_PAIDMONEY = 0,
                SO_PICKTIME = null,
                SO_DELIVERTIME = null,
                SO_PAYTIME = null,
                SO_DEALTIME = null,
                SALES_ORDER_CONDITION = null,

            };

            ctx.SALES_ORDER.Add(OraLINQ);
            ctx.SaveChanges();
            return (int)OraLINQ.SO_ID;
        }

        public void createDeliveryItem(String salesOrderID, String materialID, String amount, String itemPrice)
        {
            TJERPEntity ctx = new TJERPEntity();

            var OraLINQ = new DELIVERY_ITEM()
            {
                DI_SALESORDERID = Convert.ToDecimal(salesOrderID),
                DI_MATERIALID = Convert.ToDecimal(materialID),
                DI_PREAMOUNT = Convert.ToDecimal(amount),
                DI_ITEMPRICE = Convert.ToDecimal(itemPrice),
                DI_PICKAMOUNT = 0

            };

            ctx.DELIVERY_ITEM.Add(OraLINQ);
            ctx.SaveChanges();
        }



        public SALES_ORDER getSalesOrder(String id)
        {

            using (TJERPEntity ctx = new TJERPEntity())
            {
                var querySalesOrder = from so in ctx.SALES_ORDER
                                      select so;

                foreach (SALES_ORDER so in querySalesOrder)
                {
                    if (so.SO_ID.ToString().Equals(id))
                    {
                        return so;
                    }
                }

                return null;
            }


        }

 
        public PURCHASE_ORDER getPurchaseOrder(String id)
        {
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryPurchaseOrder = from po in ctx.PURCHASE_ORDER
                                      select po;

                foreach (PURCHASE_ORDER po in queryPurchaseOrder)
                {
                    if (po.PO_ID.Equals(id))
                    {
                        return po;
                    }
                }

                return null;
            }
        }

        public bool updateSalesOrder(String id, int status)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedOrder = ctx1.SALES_ORDER.Find(Convert.ToDecimal(id));
            updatedOrder.SO_SORDERSTATE = status;
            ctx1.SALES_ORDER.Attach(updatedOrder);
            var entry = ctx1.Entry(updatedOrder);
            entry.Property(e => e.SO_SORDERSTATE).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }


        public bool updatePurchaseOrder(String id, int status)
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updatedOrder = ctx1.PURCHASE_ORDER.Find(Convert.ToDecimal(id));
            updatedOrder.PO_PORDERSTATE = status;
            ctx1.PURCHASE_ORDER.Attach(updatedOrder);
            var entry = ctx1.Entry(updatedOrder);
            entry.Property(e => e.PO_PORDERSTATE).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }


        public List<DELIVERY_ITEM> getDeliveryItemList(String SO_ID)   
        {
            List<DELIVERY_ITEM> result = new List<DELIVERY_ITEM>();
            using (TJERPEntity ctx = new TJERPEntity())
            {
                var queryDeliveryItemListr = from so in ctx.DELIVERY_ITEM
                                             select so;
                List<DELIVERY_ITEM> queryDeliveryItemList = queryDeliveryItemListr.ToList();

                foreach (DELIVERY_ITEM di in queryDeliveryItemListr)
                {
                    if (di.DI_SALESORDERID.ToString().Equals(SO_ID))
                    {
                        result.Add(di);
                    }
                }
                return result;
            }
        }

        public bool updateDeliveryItemAmount(String DI_ID, String amount)//xinjiade
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updateDeliveryItemAmount = ctx1.DELIVERY_ITEM.Find(Convert.ToDecimal(DI_ID));
            decimal result = (decimal)updateDeliveryItemAmount.DI_PICKAMOUNT + Convert.ToDecimal(amount);
            updateDeliveryItemAmount.DI_PICKAMOUNT = result;
            ctx1.DELIVERY_ITEM.Attach(updateDeliveryItemAmount);
            var entry = ctx1.Entry(updateDeliveryItemAmount);
            entry.Property(e => e.DI_PICKAMOUNT).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }

        public bool updateMaterial(String M_ID, String amount)//xinjiade
        {
            TJERPEntity ctx1 = new TJERPEntity();
            var updateMaterial = ctx1.MATERIALs.Find(Convert.ToDecimal(M_ID));
            decimal result = (decimal)updateMaterial.M_LEFTAMOUT - Convert.ToDecimal(amount);
            updateMaterial.M_LEFTAMOUT = result;
            ctx1.MATERIALs.Attach(updateMaterial);
            var entry = ctx1.Entry(updateMaterial);
            entry.Property(e => e.M_LEFTAMOUT).IsModified = true;
            ctx1.SaveChanges();
            return true;
        }


    }
}
